// -- floating label
document.querySelectorAll('.floating-label').forEach(function(el){

	if( el.querySelector('input') != null ){
		el.setAttribute('data-placeholder',( el.querySelector('input').getAttribute('placeholder') ? el.querySelector('input').getAttribute('placeholder') : '' ));

		if( el.querySelector('input').getAttribute('type') == 'time' ){
			el.classList.add('type-time');
		}else if( el.querySelector('input').getAttribute('type') == 'date' ){
			el.classList.add('type-date');
		}else if( el.querySelector('input').getAttribute('type') == 'password' ){
			el.classList.add('type-password');
		}else if( el.querySelector('input').getAttribute('type') == 'number' ){
			el.classList.add('type-number');
		}else if( el.querySelector('input').getAttribute('type') == 'email' ){
			el.classList.add('type-email');
		}else{
			el.classList.add('type-text');
		}
		if( el.querySelector('input').getAttribute('type') != 'password' && el.querySelector('input').getAttribute('type') != 'number' && el.querySelector('input').getAttribute('type') != 'email' ){
			el.querySelector('input').setAttribute('type','text');
		}
		el.querySelector('input').setAttribute('placeholder','');
		if( el.querySelector('input').value && el.querySelector('input').value != '' ){
			el.classList.add('active');
		}
	}

	if( el.querySelector('textarea') != null ){
		el.setAttribute('data-placeholder',( el.querySelector('textarea').getAttribute('placeholder') ) ? el.querySelector('textarea').getAttribute('placeholder') : '');
		el.classList.add('type-textarea');
		el.querySelector('textarea').setAttribute('placeholder','');
		if( el.querySelector('textarea').value && el.querySelector('textarea').value != '' ){
			el.classList.add('active');
		}
		el.querySelector('textarea').addEventListener('input',function(){
			this.style.cssText = 'height:auto;width:100%';
            this.style.cssText = 'height:' + this.scrollHeight + 'px;width:100%';
		});
	}

	if( el.querySelector('select') != null ){
		el.classList.add('type-select');
		if( el.querySelector('select').value && el.querySelector('select').value != '' ){
			el.classList.add('active');
		}
	}
});


document.querySelectorAll('.input-icon').forEach(function(el){

	if( el.querySelector('input').value && el.querySelector('input').value != '' ){
		el.classList.add('active');
	}

	el.querySelector('input').addEventListener('focus',function(){
		el.classList.add('active');
	});

	el.querySelector('input').addEventListener('focusout',function(){
		if( this.value == '' ){
			el.classList.remove('active');
		}
	});

});

// -- super select
document.querySelectorAll('.super-select').forEach(function(el){
	var _select = '<div class="super-select-items-wrapper"><div style="padding:12px;border-bottom:1px solid #ededed"><input placeholder="Search..." class="form-control" style="height:initial!important;margin-bottom:8px;padding: 5px 12px !important;" type="text" autocomplete="off" value=""></div><ul>';

	el.querySelector('select').querySelectorAll('option').forEach(function(el2){
		_select+='<li><a href="#" data-value="'+el2.getAttribute('value')+'">'+el2.textContent+'</a></li>';
	});

	_select+='</ul></div>';

	var _wrapper = document.querySelector('div');
	el.innerHTML = el.innerHTML+'<div class="text-content"></div><i class="zmdi zmdi-chevron-down"></i>';

	el.querySelector('select').querySelectorAll('option').forEach(function(el2){
		if( el2.selected ){
			el.querySelector('.text-content').textContent = el2.textContent;
		}
	});

	var _dp = document.createElement('div');
	_dp.style.position = 'relative';
	_dp.style.width = '100%';
	_dp.innerHTML = _select;
	el.appendChild(_dp);


});

$(function(){
	// -- password switcher
	$('.password-switcher').each(function(){

		if( $(this).find('.switcher').hasClass('active') ){
			$(this).find('.switcher').addClass('active');
			$(this).find('.switcher').html('<i class="'+$(this).attr('data-active-icon')+'"></i>');
			$(this).find('input').attr('type','text');
		}else{
			$(this).find('.switcher').removeClass('active');
			$(this).find('.switcher').html('<i class="'+$(this).attr('data-nonactive-icon')+'"></i>');
			$(this).find('input').attr('type','password');
		}
	});

	$(document).on('click','.password-switcher .switcher',function(e){
		e.preventDefault();
		$(this).toggleClass('active');
		if( $(this).hasClass('active') ){
			$(this).closest('.password-switcher').find('input').attr('type','text');
			$(this).html('<i class="'+$(this).closest('.password-switcher').attr('data-active-icon')+'"></i>');
		}else{
			$(this).closest('.password-switcher').find('input').attr('type','password');
			$(this).html('<i class="'+$(this).closest('.password-switcher').attr('data-nonactive-icon')+'"></i>');
		}
		var _this = $(this);
	});

	// -- floating label
	$(document).on('click','.floating-label',function(){
		var _this = $(this);
		var el = '';

		$(this).addClass('active');
		
		if( $(this).hasClass('type-text') || $(this).hasClass('type-number') || $(this).hasClass('type-email') ){
			el = $(this).find('input');
		}
		if( $(this).hasClass('type-textarea') ){
			el = $(this).find('textarea');
		}

		setTimeout(function(){
			if( el != '' ){
				el.attr('placeholder',_this.attr('data-placeholder'));
			}else{
				if( _this.hasClass('type-time') ){
					_this.find('input').attr('type','time');
				}
				if( _this.hasClass('type-date') ){
					_this.find('input').attr('type','date');
				}
			}
		},200);

	});

	// -- floating label
	$(document).on('focusout','.floating-label',function(){

		var _val = '';

		if( $(this).hasClass('type-text') || $(this).hasClass('type-time') || $(this).hasClass('type-date') ){
			$(this).find('input').attr('placeholder','');
			$(this).find('input').attr('type','text');
			_val = $(this).find('input').val();
		}

		if( $(this).hasClass('type-number') || $(this).hasClass('type-email') ){
			$(this).find('input').attr('placeholder','');
			_val = $(this).find('input').val();
		}

		if( $(this).hasClass('type-select') ){
			_val = $(this).find('select').val();
		}

		if( $(this).hasClass('type-textarea') ){
			_val = $(this).find('textarea').val();
		}

		if( _val == '' ){
			$(this).removeClass('active');
		}

	});

	// -- super select
	$(document).on('click','.super-select .text-content',function(){
		if( $(this).closest('.super-select').find('.super-select-items-wrapper').is(':visible') ){
			$(this).closest('.super-select').find('.super-select-items-wrapper').fadeOut(300);
		}else{
			$(this).closest('.super-select').find('.super-select-items-wrapper').fadeIn(300);
		}
	});

	$(document).on('input','.super-select-items-wrapper input', function() {
        var value = this.value.toLowerCase();
        $(this).closest('.super-select-items-wrapper').find('ul li').hide().filter(function() {
            var name = $(this).find("a").text().toLowerCase();
            return name.indexOf(value) > -1;
      	}).fadeIn(200);
    });

    $(document).on('click','.super-select-items-wrapper:visible ul li a',function(e){
    	e.preventDefault();

    	$(this).closest('.super-select').find('.text-content').text($(this).text());

    	$(this).closest('.super-select').find('select').val($(this).attr('data-value'))
    	.find('option[value="'+$(this).attr('data-value')+'"]')
    	.prop('selected',true)
    	.closest('select')
    	.trigger('change');

    	$(this).closest('.super-select-items-wrapper').fadeOut(300);

    });

	$(document).on("mousedown touchstart", function(e) {
        var _dp = $('.super-select-items-wrapper');
        if (!_dp.is(e.target) && _dp.has(e.target).length == 0) {
            _dp.fadeOut(300);
        }
    });

    // -- tab
    $('.tab').each(function(e){
    	var _this = $(this);
    	$(this).append('<div class="tab--header"><ul></ul></div>');
    	$(this).append('<div class="tab--content"></div>');
    	$(this).find('.tab-header').each(function(f){
    		var _active = ( $(this).hasClass('active') ) ? 'active' : '';
    		_this.find('.tab--header ul').append('<li class="'+_active+'"><a href="#tab-'+e+'-'+f+'" class="ripple">'+$(this).html()+'</a></li>');

    		if( _active != '' ){
    			$(this).next().addClass('active').attr('id','tab-'+e+'-'+f).appendTo( _this.find('.tab--content') );
    		}else{
    			$(this).next().attr('id','tab-'+e+'-'+f).appendTo( _this.find('.tab--content') );
    		}

    		$(this).remove();
    	});

    	if( $(this).find('.tab--header li.active').length == 0 ){
    		$(this).find('.tab--header li:first-child').addClass('active');
    		$(this).find('.tab--content .tab-content:nth-child(1)').addClass('active');
    	}
    });

    $(document).on('click','.tab .tab--header ul li a',function(e){
    	e.preventDefault();

    	$(this).closest('ul').find('li.active').removeClass('active');
    	$(this).closest('li').addClass('active');
    	$(this).closest('.tab').find('.tab-content.active').removeClass('active');

    	$(this).closest('.tab').find('.tab-content'+$(this).attr('href') ).addClass('active');
    });
    // -- fomr validator
    var validateEmail = function(email){
        var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        return regex.test(email);
    }
    $('.validate').each(function(){
    	if( $(this).closest('.input-icon').length > 0){
    		$(this).wrap('<div class="field-wrapper"></div>');
    		$(this).closest('.input-icon').find('.icon').appendTo($(this).closest('.input-icon').find('.field-wrapper'));
    	}
		if( !$(this).attr('data-error') ){
			if( $(this).closest('.super-select').length > 0 ){
				$(this).closest('.super-select').after('<div class="error-holder"></div>');
			}else{
				if( $(this).closest('.input-icon').length > 0){
					$(this).closest('.field-wrapper').after('<div class="error-holder"></div>');
				}else{
					$(this).after('<div class="error-holder"></div>');
				}
				
			}
		}

		if( $(this).attr('data-error') ){
			if( $( $(this).attr('data-error') ).find('.error.required').length == 0 ){
				$( $(this).attr('data-error') ).append('<div class="error required"></div>');
			}
			if( $(this).hasClass('email') ){
				if( $( $(this).attr('data-error') ).find('.error.email').length == 0 ){
					$( $(this).attr('data-error') ).append('<div class="error email"></div>');
				}
			}
			if( $(this).hasClass('number') ){
				if( $( $(this).attr('data-error') ).find('.error.number').length == 0 ){
					$( $(this).attr('data-error') ).append('<div class="error number"></div>');
				}
			}

			if(  $(this).hasClass('cpassword') && $(this).attr('data-password') ){
				if( $( $(this).attr('data-error') ).find('.error.cpassword').length == 0 ){
					$( $(this).attr('data-error') ).append('<div class="error cpassword"></div>');
				}
			}
		}

		if( $(this).hasClass('required') && $.trim($(this).val()) == '' ){
    		$(this).addClass('error');
    	}
    	if( $(this).attr('type') == 'pasword' && $(this).attr('data-password') ){
    		if( $.trim($(this).val()) != $.trim( $( $(this).attr('data-password') ).val() ) ){
    			$(this).addClass('error');
    		}
    	}

    	if( $(this).hasClass('email') && $.trim($(this).val()) != '' && !validateEmail($(this).val()) ){
    		$(this).addClass('error');
    	}

    	if( $(this).hasClass('number') && $.trim($(this).val()) != '' && !$.isNumeric( $(this).val() ) ){
    		$(this).addClass('error');
    	}
	});
    $(document).on('focus','input.validate,textarea.validate',function(){
    	if( $(this).closest( '.input-icon').length > 0 ){
    		$(this).closest( '.input-icon').addClass('active');
    	}


    });
   //  $(document).on('keyup','input.cpassword',function(){

   //  	if( $.trim($(this).val()) == $.trim( $( $(this).attr('data-password') ).val() ) ){
   //  		if($( $(this).attr('data-password') ).attr('data-error') ){

			// 	if( $( $( $(this).attr('data-password') ).attr('data-error') ).find('.error.active').length > 0  ){
		 //    		$( $( $(this).attr('data-password') ).attr('data-error') ).show();
		 //    	}else{
		 //    		$( $( $(this).attr('data-password') ).attr('data-error') ).hide();
		 //    	}
			    
			// }else{

			// 	$( $(this).attr('data-password') ).next('.error-holder').find('.form-error.cpassword').remove();
			// }
   //  	}

   //  });
    $(document).on('focusout','input.validate,textarea.validate',function(){

    	if( $(this).closest( '.input-icon').length > 0 ){
    		$(this).closest( '.input-icon').removeClass('active');
    	}
    	if( $(this).hasClass('required') ){
    		if( $.trim($(this).val()) == '' ){
	    		var _name = 'input';
	    		$(this).addClass('error');
    			if( $(this).attr('data-alias') ){
    				_name = $(this).attr('data-alias');
    			}
	    		if( $(this).attr('data-error') ){

	    			$($(this).attr('data-error')).find('.error').removeClass('active').hide();
	    			if( $.trim($($(this).attr('data-error')).find('.error.required').html() ) == '' ){

	    				$($(this).attr('data-error')).find('.error.required').text(_name+' is required');
	    			}
	    			$($(this).attr('data-error')).find('.error.required').addClass('active').show();

	    		}else{
	    			if( $(this).closest('.input-icon').length > 0){
	    				$(this).closest('.input-icon').find('.form-error.required').remove();
						$(this).closest('.input-icon').find('.error-holder').append('<p class="form-error required">'+_name+' is required</p>');	
	    			}else{
	    				$(this).next('.error-holder').find('.form-error.required').remove();
	    				$(this).next('.error-holder').append('<p class="form-error required">'+_name+' is required</p>');	
	    			}
	    		}
	    	}else{
	    		$(this).removeClass('error');
	    		if( !$(this).attr('data-error') ){
	    			if( $(this).closest('.input-icon').length > 0){
	    				$(this).closest('.input-icon').find('.form-error.required' ).remove();
	    			}else{
	    				$(this).next('.error-holder').find('.form-error.required' ).remove();
	    			}
	    		}else{
	    			$($(this).attr('data-error')).find('.error.required').removeClass('active').hide();
	    		}
	    	}
    	}

    	if( $.trim( $(this).val() ) != '' && $(this).hasClass('cpassword') && $(this).attr('data-password') ){

    		if( $.trim($(this).val()) != $.trim( $( $(this).attr('data-password') ).val() ) ){
	    		var _name = 'Confirm password';
	    		$(this).addClass('error');
    			if( $(this).attr('data-alias') ){
    				_name = $(this).attr('data-alias');
    			}
	    		if( $(this).attr('data-error') ){
	    			$($(this).attr('data-error')).find('.error').removeClass('active').hide();
	    			if( $.trim($($(this).attr('data-error')).find('.error.cpassword').html() ) == '' ){
	    				$($(this).attr('data-error')).find('.error.cpassword').text(_name+' does not match with the password field');
	    			}
	    			$($(this).attr('data-error')).find('.error.cpassword').addClass('active').show();

	    		}else{
	    			if( $(this).closest('.input-icon').length > 0){	
	    				$(this).closest('.input-icon').find('.form-error.cpassword').remove();
		    			$(this).closest('.input-icon').find('.error-holder').append('<p class="form-error cpassword">'+_name+' does not match with the password field</p>');
	    			}else{
	    				$(this).next('.error-holder').find('.form-error.cpassword').remove();
		    			$(this).next('.error-holder').append('<p class="form-error cpassword">'+_name+' does not match with the password field</p>');
	    			}
	    		}
	    	}else{


	    		if( !$(this).attr('data-error') ){
	    			if( $(this).closest('.input-icon').length > 0){
	    				$( $(this).attr('data-password') ).closest('.input-icon').find('.form-error.cpassword').remove();
	    				$(this).closest('.input-icon').find('.form-error.cpassword' ).remove();
	    			}else{
	    				$( $(this).attr('data-password') ).next('.error-holder').find('.form-error.cpassword').remove();
	    				$(this).next('.error-holder').find('.form-error.cpassword' ).remove();
	    			}


	    		}else{
	    			$($(this).attr('data-error')).find('.error.cpassword').removeClass('active').hide();
	    		}
	    	}
    	}

    	if( $(this).hasClass('email') && $.trim($(this).val()) != '' ){

    		if( !validateEmail( $(this).val() ) ){
    			$(this).addClass('error');
	    		
	    		var _name = 'input email';
    			if( $(this).attr('data-alias') ){
    				_name = $(this).attr('data-alias');
    			}
	    		if( $(this).attr('data-error') ){
	    			$($(this).attr('data-error')).find('.error').removeClass('active').hide();
	    			if( $.trim($($(this).attr('data-error')).find('.error.email').html() ) == '' ){
	    				$($(this).attr('data-error')).find('.error.email').text(_name+' must be valid');
	    			}
	    			$($(this).attr('data-error')).find('.error.email').addClass('active').show();

	    		}else{
	    			if( $(this).closest('.input-icon').length > 0){
	    				$(this).closest('.input-icon').find('.form-error.email').remove();
		    			$(this).closest('.input-icon').find('.error-holder').append('<p class="form-error email">'+_name+' is not a valid email</p>');
	    			}else{
	    				$(this).next('.error-holder').find('.form-error.email').remove();
		    			$(this).next('.error-holder').append('<p class="form-error email">'+_name+' is not a valid email</p>');
	    			}
	    		}
	    	}else{
	    		$(this).removeClass('error');

	    		if( !$(this).attr('data-error') ){
	    			if( $(this).closest('.input-icon').length > 0){
	    				$(this).closest('.input-icon').find('.form-error.email' ).remove();
	    			}else{
	    				$(this).next('.error-holder').find('.form-error.email' ).remove();
	    			}
	    			
	    		}else{
	    			$($(this).attr('data-error')).find('.error.email').removeClass('active').hide();
	    		}
	    	}
    	}

    	if( $(this).hasClass('number') && $.trim($(this).val()) != '' ){
    		if( !$.isNumeric( $(this).val() ) ){
    			$(this).addClass('error');

    			var _name = 'input number';
    			if( $(this).attr('data-alias') ){
    				_name = $(this).attr('data-alias');
    			}
	    		if( $(this).attr('data-error') ){
	    			$($(this).attr('data-error')).find('.error').removeClass('active').hide();
	    			if( $.trim($($(this).attr('data-error')).find('.error.number').html() ) == '' ){
	    				$($(this).attr('data-error')).find('.error.number').text(_name+' must be valid');
	    			}
	    			$($(this).attr('data-error')).find('.error.number').addClass('active').show();
	    		}else{
	    			if( $(this).closest('.input-icon').length > 0){
	    				$(this).closest('.input-icon').find('.form-error.number').remove();
		    			$(this).closest('.input-icon').find('.error-holder').append('<p class="form-error number">'+_name+' must contains only numbers</p>');
	    			}else{
	    				$(this).next('.error-holder').find('.form-error.number').remove();
		    			$(this).next('.error-holder').append('<p class="form-error number">'+_name+' must contains only numbers</p>');
	    			}
	    		}
	    	}else{
	    		$(this).removeClass('error');

	    		if( !$(this).attr('data-error') ){
	    			if( $(this).closest('.input-icon').length > 0){
	    				$(this).closest('.input-icon').find('.form-error.number' ).remove();
	    			}else{
	    				$(this).next('.error-holder').find('.form-error.number' ).remove();
	    			}
	    		}else{
	    			$($(this).attr('data-error')).find('.error.number').removeClass('active').hide();
	    		}
	    	}
    	}

    	
    	if( $(this).attr('data-error') && $($(this).attr('data-error')).find('.error.active').length > 0 ){
    		$($(this).attr('data-error')).show();
    	}else{
    		$($(this).attr('data-error')).hide();
    	}


    });
    $(document).on('change','select.validate',function(){

    	if( $.trim($(this).val()) == '' ){
    		$(this).addClass('error');

    		var _name = 'Option';
			if( $(this).attr('data-alias') ){
				_name = $(this).attr('data-alias');
			}
    		if( $(this).attr('data-error') ){
    			$($(this).attr('data-error')).find('.error').hide();
    			if( $.trim($($(this).attr('data-error')).find('.error.required') ) == '' ){
    				$($(this).attr('data-error')).find('.error.required').text(_name+' is required');
    			}
    			$($(this).attr('data-error')).find('.error.required').show();

    			$($(this).attr('data-error')).show();
    		}else{
    			if( $(this).closest('.super-select').length > 0 ){
    				$(this).closest('.super-select').next('.error-holder').find('.form-error.required').remove();
	    			$(this).closest('.super-select').next('.error-holder').append('<p class="form-error required">'+_name+' is required</p>');    			
    			}else{
    				$(this).next('.error-holder').find('.form-error.required').remove();
	    			$(this).next('.error-holder').append('<p class="form-error required">'+_name+' is required</p>');    			
    			}
    		}
    	}else{
    		$(this).removeClass('error');
    		
    		if( $(this).attr('data-error') ){
    			$($(this).attr('data-error')).hide();
    		}else{
    			if( $(this).closest('.super-select').length > 0 ){
    				$(this).next('.error-holder').find('.form-error.required' ).remove();
    			}else{
    				$(this).next('.error-holder').find('.form-error.required' ).remove();
    			}
    		}
    	}

    });
    // -- end validator
});

// -- parallax
function isInViewport(node) {
  var rect = node.getBoundingClientRect()
  return (
    (rect.height > 0 || rect.width > 0) &&
    rect.bottom >= 0 &&
    rect.right >= 0 &&
    rect.top <= (window.innerHeight || document.documentElement.clientHeight) &&
    rect.left <= (window.innerWidth || document.documentElement.clientWidth)
  )
}

$(function(){
	$(window).scroll(function() {
		
	});
	// -- flexslider extension
	$('.bg-image').each(function(){
		if( $(this).find('ul.slides li').length > 0 ){
			$(this).find('ul.slides li .background-image-holder').each(function(){
				$(this).css('background-image','url('+$(this).find('img').attr('src')+')');
				$(this).find('img').hide();
			});
		}else{
			$(this).find('.background-image-holder').css('background-image','url('+$(this).find('.background-image-holder img').attr('src')+')');
			$(this).find('.background-image-holder img').hide();
		}
	});
	$('.slider').each(function(){

		$(this).css({
			'border' : 'none',
			'overflow' : 'hidden'
		});

		var _options = {
			animation: "fade",
			controlNav: false,
			direction: "horizontal",
			smoothHeight : true,
			startAt : 0,
			slideshowSpeed : 7000,
			animationSpeed : 600,
			randomize : false,
			before: function(slider){
		      	$(slider).find(".flex-active-slide").find('.caption-inner').each(function(){
		       		$(this).removeClass('animated '+$(this).attr('data-in'));
		       	});
		      	if( $(slider).find(".flex-active-slide").is(':last-child') ){
		      		$(slider).find("li:first-child").find('.caption-inner').hide();
		      	}else{
		      		$(slider).find(".flex-active-slide").next('li').find('.caption-inner').hide();
		      	}
		       	
		     },
		    after: function(slider){
		        $(slider).find(".flex-active-slide").find('.caption-inner').each(function(){
		        	var $this = $(this);
		       		if( $(this).attr('data-delay') ){
		       			$this.addClass('animated '+$this.attr('data-in')).css({
		       				'-webkit-animation-delay' : parseInt($this.attr('data-delay'))+'ms',
		       				'-moz-animation-delay' : parseInt($this.attr('data-delay'))+'ms',
		       				'-o-animation-delay' : parseInt($this.attr('data-delay'))+'ms',
		       				'animation-delay' : parseInt($this.attr('data-delay'))+'ms',
		       				'-webkit-animation-duration' : '400ms',
		       				'-moz-animation-duration' : '400ms',
		       				'-o-animation-duration' : '400ms',
		       				'animation-duration' : '400ms',

		       			});
		       			// setTimeout(function(){
		       			// 	$this.addClass('animated '+$this.attr('data-in')).show();
		       			// },parseInt($this.attr('data-delay')));
		       		}

		       		if( $(this).attr('data-duration') ){
		       			$this.addClass('animated '+$this.attr('data-in')).css({
		       				'-webkit-animation-duration' : parseInt($this.attr('data-duration'))+'ms',
		       				'-moz-animation-duration' : parseInt($this.attr('data-duration'))+'ms',
		       				'-o-animation-duration' : parseInt($this.attr('data-duration'))+'ms',
		       				'animation-duration' : parseInt($this.attr('data-duration'))+'ms',
		       			});
		       		}else{
		       			$this.addClass('animated '+$this.attr('data-in')).css({
		       				'-webkit-animation-duration' : '400ms',
		       				'-moz-animation-duration' : '400ms',
		       				'-o-animation-duration' : '400ms',
		       				'animation-duration' : '400ms',

		       			});
		       		}

		       		$this.addClass('animated '+$this.attr('data-in')).show();
		       	});
		    },

		}



		if( $(this).attr('data-options') ){

			var obj = {};
			var obj1 = $(this).attr('data-options').replace('{','').replace('}','').split(',');

			obj1.forEach(function(el){
				obj[el.split(':')[0].trim()] = el.split(':')[1].trim().replace(/'/g,''); 
			});
					
			for (var key in obj){
				if (obj.hasOwnProperty(key)) {
			        _options[key] = obj[key];
			    }
			}

		}

		_options.animation = 'fade';

		$(this).attr('id','slider'+$(this).index());

		$(this).flexslider(_options);
	});
	// -- end flexslider
});
// -- sidebar menu
function animateCSS(element, animationName, callback) {
    const node = document.querySelector(element)
    node.classList.add('animated', animationName)

    function handleAnimationEnd() {
        node.classList.remove('animated', animationName)
        node.removeEventListener('animationend', handleAnimationEnd)

        if (typeof callback == 'function') callback()
    }

    node.addEventListener('animationend', handleAnimationEnd)
}

$(function(){
	$('.sidebar-toggle-menu').click(function(e){
		e.preventDefault();

		if( !$('#sidebar-menu-box').hasClass('active')){
			if( $('#sidebar-menu-box').hasClass('left-open') ){
				$('#sidebar-menu-box').addClass('animated slideInLeft active').show();
			}else{
				$('#sidebar-menu-box').addClass('animated slideInRight active').show();
			}
		}else{
			if( $('#sidebar-menu-box').hasClass('left-open') ){
				animateCSS('#sidebar-menu-box','slideOutLeft',function(){
					$('#sidebar-menu-box').removeClass('active').hide();
				});
			}else{
				animateCSS('#sidebar-menu-box','slideOutRight',function(){
					$('#sidebar-menu-box').removeClass('active').hide();
				});	
			}
		}
		
	});

	$('.sidebar-menu-close').click(function(e){
		e.preventDefault();
		if( $('#sidebar-menu-box').hasClass('left-open') ){
			animateCSS('#sidebar-menu-box','slideOutLeft',function(){
				$('#sidebar-menu-box').removeClass('active').hide();
			});
		}else{
			animateCSS('#sidebar-menu-box','slideOutRight',function(){
				$('#sidebar-menu-box').removeClass('active').hide();
			});	
		}
	});
});

// -- move box
$(function(){

	if( $('.move-box').length > 0 && $(window).width() > 767 ){
		$(window).mousemove(function(e) {
		    var change;
		    var xpos=e.clientX;var ypos=e.clientY;
		    var left= change*20;
		    var  xpos=xpos*2;ypos=ypos*2;
		    $('.move-box.active').css('top',((0+(ypos/50))+"px"));
		    $('.move-box.active').css('right',(( 0+(xpos/80))+"px"));
		                   
		});

		// get top positions and references to all images
		var pos = $(".move-box").map(function(){
		  var $this = $(this);
		  return {
		    el: $this,
		    top: $this.offset().top
		  };
		}).get();

		// provide document scrolling
		$(document).on("scroll", function() {

		  $(".move-box").removeClass("active");

		  var scroll = $(this).scrollTop();
		  var i = 0;
		  while(pos[i].top < scroll) i++;

		  pos[i].el.addClass("active");

		  // -- parallax image bg
		  	if( $('.bg-image.parallax .background-image-holder').length > 0 && $(window).width() > 767 ){
		  		var scrolled = $(window).scrollTop();
		    	$('.bg-image.parallax .background-image-holder').each(function(index, element) {
			    	var initY = $(this).offset().top;
			    	var height = $(this).height();
			    	var endY  = initY + $(this).height();

			    	// Check if the element is in the viewport.
			    	var visible = isInViewport(this);
			    	if(visible) {
			        	var diff = scrolled - initY;
			        	var ratio = Math.round((diff / height) * 100);
			        	$(this).css('background-position','center ' + parseInt(-(ratio * 1.5)) + 'px');
			      	}
			    });
		  	}

		  	// -- scroll menu
		  	if( $(window).width() > 767 ){	  		

		      	if( $(window).scrollTop() > 150 && !$('#top-header').hasClass('hidden') ){
		        	$('#top-header').addClass('hidden active').animate({
		            	top : -parseInt($('#nav1').height())+'px'
		          	},300);
		          	setTimeout(function(){
		            	$('#top-header').addClass('active');
		          	},300);
		          	
		      	}

		      	if( $(window).scrollTop() < 130 && $('#top-header').hasClass('hidden') ){
		        	$('#top-header').removeClass('hidden').animate({              
		            	top : '0px'
		          	},300);

		          	setTimeout(function(){
		            	$('#top-header').removeClass('active');
		          	},300);

		          	anim = false;
		    	}
		  	}

		}).scroll();
	}

});

//-- modal
$(function(){
	if( $( '#theme-modal' ).length == 0 ){
		$('body').append('<div class="md-container d-flex align-items-center" id="theme-modal"><div class="md-content"></div></div><div class="md-overlay"></div>');
	}	
});

// -- modal plugin
;(function($) {
  'use strict';

  var defaults = {
    closeSelector: '.md-close',
    contentSelector: '.md-content',
    classAddAfterOpen: 'md-show',
    classScrollbarMeasure: 'md-scrollbar-measure',
    classModalOpen: 'md-open',
    data: false,
    buttons: false,
    beforeOpen: false,
    afterOpen: false,
    beforeClose: false,
    afterClose: false
  };

  $.fn.niftyModal = function(method) {

    var config = {};
    var modal = {};
    var body = $('body');
    var bodyIsOverflowing, scrollbarWidth, originalBodyPad;

    var helpers = {

      removeModal: function( m ) {
        var mod = $( m );
        mod.removeClass( config.classAddAfterOpen );
        mod.css({'perspective':'1300px'});
        //Remove body open modal class
        body.removeClass(config.classModalOpen);
        //Reset body scrollbar padding
        helpers.resetScrollbar();
        mod.trigger('hide');
      },    
      showModal: function( m ) {
        var mod = $(m);
        var close = $(config.closeSelector, m);

        //beforeOpen event
        if( typeof config.beforeOpen == 'function' ){
          if( config.beforeOpen( modal ) == false){
            return false;
          }
        }

        //Calculate scrollbar width
        helpers.checkScrollbar();
        helpers.setScrollbar();

        //Make the modal visible
        mod.addClass( config.classAddAfterOpen );

        //Add body open modal class
        body.addClass( config.classModalOpen );

        //Close on click outside the modal
        $( mod ).on('click', function ( e ) {
          
          var _mContent = $(config.contentSelector, mod);
          var close = $(config.closeSelector, mod);

          if ( !$( e.target ).closest( _mContent ).length && body.hasClass( config.classModalOpen ) ) {

            //Before close event
            if( typeof config.beforeClose == 'function' ){
              if( config.beforeClose(modal, e) == false ){
                return false;
              }
            }
            
            helpers.removeModal(mod);
            close.off('click');

            //After close event
            if( typeof config.afterClose == 'function' ){
              config.afterClose(modal, e);
            }
          }
        });

        //After open event
        if( typeof config.afterOpen == 'function' ){
          config.afterOpen( modal );
        }

        setTimeout( function() {
          mod.css({'perspective':'none'});
        }, 500 ); 
        
        //Close Event
        $(document).on( 'click',config.closeSelector, function( ev ) {
        	ev.preventDefault();
          var btn = $(this);
          modal.closeEl = close.get( 0 );

          //Before close event
          if( typeof config.beforeClose == 'function' ){
            if( config.beforeClose(modal, ev) == false ){
              return false;
            }
          }  

          //Buttons callback
          if( config.buttons && $.isArray( config.buttons ) ){
            var cancel = true;
            
            $.each(config.buttons, function( i, v){
              if( btn.hasClass( v.class ) && typeof v.callback == 'function' ){
                cancel = v.callback( btn.get( 0 ), modal, ev);
              }
            });

            if( cancel == false && typeof cancel !== undefined ){
              return false;
            }
          }  
        
          helpers.removeModal( m );
          close.off('click');

          //After close event
          if( typeof config.afterClose == 'function' ){
            config.afterClose( modal, ev);
          }

          ev.stopPropagation();
        });
        
        mod.trigger('show');
      },
      measureScrollbar: function() {
        var scrollDiv = document.createElement('div');
        scrollDiv.className = config.classScrollbarMeasure;
        body.append(scrollDiv);
        var scrollbarWidth = scrollDiv.offsetWidth - scrollDiv.clientWidth;
        body[0].removeChild(scrollDiv);
        return scrollbarWidth;
      },
      checkScrollbar: function() {
        var fullWindowWidth = window.innerWidth;
        if (!fullWindowWidth) { // workaround for missing window.innerWidth in IE8
          var documentElementRect = document.documentElement.getBoundingClientRect();
          fullWindowWidth = documentElementRect.right - Math.abs(documentElementRect.left);
        }
        bodyIsOverflowing = document.body.clientWidth < fullWindowWidth;
        scrollbarWidth = helpers.measureScrollbar();
      },
      setScrollbar: function() {
        var bodyPad = parseInt((body.css('padding-right') || 0), 10);
        originalBodyPad = document.body.style.paddingRight || '';
        if (bodyIsOverflowing){
          body.css('padding-right', bodyPad + scrollbarWidth);
        }
      },
      resetScrollbar: function() {
        body.css('padding-right', originalBodyPad);
      }

    };

    var methods = {

      init : function( options ) {

        return this.each(function() {
          config = $.extend({}, defaults, options);

          modal.modalEl = this;

          if( config.data !== false ){
            modal.data = options.data;
          }

          //Show modal
          helpers.showModal( this );
        });

      },
      toggle: function(options) {
        return this.each(function() {
          config = $.extend({}, defaults, options);
          var modal = $(this);
          if(modal.hasClass(config.classAddAfterOpen)){
            helpers.removeModal(modal);
          }else{
            helpers.showModal(modal);
          }
        });
      },
      show: function(options) {
        config = $.extend({}, defaults, options);
        return this.each(function() {

          var mod = $( this );

          //Show the modal
          helpers.showModal( mod );
          
        });
      },
      hide: function(options) {
        config = $.extend({}, defaults, options);
        return this.each(function() {
          helpers.removeModal($(this));  
        });            
      },
      setDefaults: function( options ) {
        defaults = $.extend({}, defaults, options);
      },
      getDefaults: function( ) {
        return defaults;
      }
    };

    if (methods[method]) {
        return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
    } else if (typeof method == 'object' || !method) {
        return methods.init.apply(this, arguments);
    } else {
        $.error( 'Method "' +  method + '" does not exist in niftyModal plugin!');
    }

  };

})(jQuery);

/**
 * Self execute to support previous versions with 'md-trigger' class & data-modal attribute
 */

$('.md-trigger').on('click',function(){
  var modal = 'theme-modal';
  var $this = $(this);
  var effect = 'md-effect-1';

  if( $(this).attr('data-effect') ){
  	switch($(this).attr('data-effect')){
  		case 'fade-in-scale':
  			effect = 'md-effect-1';
  		break;
  		case 'slide-in-right':
  			effect = 'md-effect-2';
  		break;
  		case 'slide-in-bottom':
  			effect = 'md-effect-3';
  		break;
  		case 'newspaper':
  			effect = 'md-effect-4';
  		break;
  		case 'fall':
  			effect = 'md-effect-5';
  		break;
  		case 'side-fall':
  			effect = 'md-effect-6';
  		break;
  		case 'sticky-up':
  			effect = 'md-effect-7';
  		break;
  		case '3d-flip-h':
  			effect = 'md-effect-8';
  		break;
  		case '3d-flip-v':
  			effect = 'md-effect-9';
  		break;
  		case '3d-sign':
  			effect = 'md-effect-10';
  		break;
  		case 'super-scaled':
  			effect = 'md-effect-11';
  		break;
  		case 'just-me':
  			effect = 'md-effect-12';
  		break;
  		case '3d-slit':
  			effect = 'md-effect-13';
  		break;
  		case '3d-rotate-bottom':
  			effect = 'md-effect-14';
  		break;
  		case '3d-rotate-left':
  			effect = 'md-effect-15';
  		break;
  		case 'blur':
  			effect = 'md-effect-16';
  		break;
  		case 'letme-in':
  			effect = 'md-effect-17';
  		break;
  		case 'make-way':
  			effect = 'md-effect-18';
  		break;
  		case 'slip-top':
  			effect = 'md-effect-19';
  		break;
  	}
  }

  	$('.md-container')
	  	.removeClass($('.md-container').attr('data-effect'))
	  	.addClass(effect)
	  	.attr('data-effect',effect);

	if( $('#theme-modal .md-content').attr('data-class') ){
	 	$('#theme-modal .md-content').removeClass($('#theme-modal .md-content').attr('data-class'));
	}

	$('#theme-modal .md-content').addClass($('#'+$(this).attr('data-content') ).attr('class'));
	$('#theme-modal .md-content').attr('data-class',$('#'+$(this).attr('data-content') ).attr('class'));

  	$('#theme-modal .md-content').html( $('#'+$(this).attr('data-content') ).html() );
  
  setTimeout(function(){
  	$("#theme-modal").niftyModal({
  	overlaySelector: '.md-overlay',//Modal overlay class
    closeSelector: '.md-close',//Modal close element class
    classAddAfterOpen: 'md-show',//Body control class
    beforeOpen: function( modal ){
      if( $this.attr('data-beforeopen') ){
      	window($this.attr('data-beforeopen'));
      }
    },
    afterOpen: function( modal ){
    	// add close btn
    	$('.md-content a.md-cose').remove();
    	$('.md-content').prepend('<a href="#" onmouseover="this.style.textDecoration = \'none\'" class="md-close" style="z-index:999;position: absolute;top: 16px;right: 16px;color: inherit;text-decoration:none;"><i class="ti-close" style="font-size:16px;"></i></a>');
      if( $this.attr('data-afteropen') ){
      	window($this.attr('data-afteropen'));
      }
    },
    beforeClose: function( modal ){
      if( $this.attr('data-beforeclose') ){
      	window($this.attr('data-beforeclose'));
      }
    },
    afterClose: function( modal ){
      if( $this.attr('data-afterclose') ){
      	window($this.attr('data-afterclose'));
      }
    }
  });	
  },100);
});

/* 
    C O M P O N E N T S  H E L P E R S
--------------------------------------------------------------------    

*/
if ('undefined' != typeof window.jQuery ) {

	function spinnerHTML(s){
		var s = parseInt(s);
		switch(s){
            case 1 :
                return '<div class="spinner-wrapper"><div class=" display-table center animated zoomIn" style="-webkit-animation-duration: 450ms;animation-duration: 450ms;" id="spinner"><div class="spinner1"></div></div>';
                break;
            case 2 :
                return '<div class="spinner-wrapper"><div class=" display-table center animated zoomIn" style="-webkit-animation-duration: 450ms;animation-duration: 450ms;" id="spinner"><div class="spinner2"><div class="double-bounce1"></div><div class="double-bounce2"></div></div></div></div>';
                break;
            case 3 :
                return '<div class="spinner-wrapper"><div class=" display-table center animated zoomIn" style="-webkit-animation-duration: 450ms;animation-duration: 450ms;" id="spinner"><div class="spinner3"><div class="cube1"></div><div class="cube2"></div></div></div></div>';
                break;
            case 4 :
                return '<div class="spinner-wrapper"><div class=" display-table center animated zoomIn" style="-webkit-animation-duration: 450ms;animation-duration: 450ms;" id="spinner"><div class="spinner4"></div></div></div>';
                break;
            case 5 :
                return '<div class="spinner-wrapper"><div class=" display-table center animated zoomIn" style="-webkit-animation-duration: 450ms;animation-duration: 450ms;" id="spinner"><div class="spinner5"><div class="dot1"></div><div class="dot2"></div></div></div></div>';
                break;
            case 6 :
                return '<div class="spinner-wrapper"><div class=" display-table center ranimated zoomIn" style="-webkit-animation-duration: 450ms;animation-duration: 450ms;" id="spinner"><div class="spinner6"><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></div></div></div>';
                break;
            case 7 :
                return '<div class="spinner-wrapper"><div class=" display-table center animated zoomIn" style="-webkit-animation-duration: 450ms;animation-duration: 450ms;" id="spinner"><div class="spinner7"><div class="sk-circle1 sk-child"></div><div class="sk-circle2 sk-child"></div><div class="sk-circle3 sk-child"></div><div class="sk-circle4 sk-child"></div><div class="sk-circle5 sk-child"></div><div class="sk-circle6 sk-child"></div><div class="sk-circle7 sk-child"></div><div class="sk-circle8 sk-child"></div><div class="sk-circle9 sk-child"></div><div class="sk-circle10 sk-child"></div><div class="sk-circle11 sk-child"></div><div class="sk-circle12 sk-child"></div></div></div></div>';
                break;
            case 8 :
                return '<div class="spinner-wrapper"><div class=" display-table center animated zoomIn" style="-webkit-animation-duration: 450ms;animation-duration: 450ms;" id="spinner"><div class="spinner8"><div class="sk-cube sk-cube1"></div><div class="sk-cube sk-cube2"></div><div class="sk-cube sk-cube3"></div><div class="sk-cube sk-cube4"></div><div class="sk-cube sk-cube5"></div><div class="sk-cube sk-cube6"></div><div class="sk-cube sk-cube7"></div><div class="sk-cube sk-cube8"></div><div class="sk-cube sk-cube9"></div></div></div></div>';
                break;
            case 9 :
                return '<div class="spinner-wrapper"><div class=" display-table center animated zoomIn" style="-webkit-animation-duration: 450ms;animation-duration: 450ms;" id="spinner"><div class="spinner9"><div class="sk-circle1 sk-circle"></div><div class="sk-circle2 sk-circle"></div><div class="sk-circle3 sk-circle"></div><div class="sk-circle4 sk-circle"></div><div class="sk-circle5 sk-circle"></div><div class="sk-circle6 sk-circle"></div><div class="sk-circle7 sk-circle"></div><div class="sk-circle8 sk-circle"></div><div class="sk-circle9 sk-circle"></div><div class="sk-circle10 sk-circle"></div><div class="sk-circle11 sk-circle"></div><div class="sk-circle12 sk-circle"></div></div></div></div>';
                break;
            case 10 :
                return '<div class="spinner-wrapper"><div class=" display-table center animated zoomIn" style="-webkit-animation-duration: 450ms;animation-duration: 450ms;" id="spinner"><div class="spinner10"><div class="sk-cube1 sk-cube"></div><div class="sk-cube2 sk-cube"></div><div class="sk-cube4 sk-cube"></div><div class="sk-cube3 sk-cube"></div></div></div></div>';
                break;
            default :
                return '<div class="spinner-wrapper"><div class="display-table center animated zoomIn" style="-webkit-animation-duration: 450ms;animation-duration: 450ms;" id="spinner"><div class="spinner11"><div class="rect1"></div><div class="rect2"></div><div class="rect3"></div><div class="rect4"></div><div class="rect5"></div></div></div></div>';
        }
	}
    function spinner(status,spinner,txt){
       if(status=="on"){
            $('body').append('<div style="display:flex;align-items:center;justify-content:center;position:fixed;z-index:9998;background:rgba(255,255,255,0.5);width:100%;height:100vh;top:0px;right:0px;" id="spinner-wrapper">'+spinnerHTML(spinner)+'</div>');
            if( typeof txt != typeof undefined ){
                $('#spinner-wrapper .spinner-wrapper').append('<div class="text-center mt16 font10">'+txt+'</div>');
            }
            $('#spinner').show();
       }else{
            $('#spinner').fadeOut(400,function(){
                $('#spinner').remove();
            });
            $('#spinner-wrapper').remove();
       }
    }

    // J - E Q U A L  C O M P O N E N T
    function equal(){

    	$('.eq').each(function(){
    		if( parseInt($(this).css('height')) == 0 ){
    			$(this).css( 'height' , 'auto' );
    		}
    	});
        
        if($(window).width()>=992){
            $('.eq-container').each(function(){
                var dis = $(this),
                    elementHeights = false,
                    maxHeight = false;
                // Get an array of all element heights
                elementHeights = dis.find('.eq').map(function() {
                    return $(this).outerHeight();
                }).get();
                // Math.max takes a variable number of arguments
                // `apply` is equivalent to passing each height as an argument
                maxHeight = Math.max.apply(null, elementHeights);
                // Set each height to the max height                
                dis.find('.eq').css('height',maxHeight);


                // j equal 2
                // Get an array of all element heights
                elementHeights = dis.find('.eq2').map(function() {
                    return $(this).outerHeight();
                }).get();
                // Math.max takes a variable number of arguments
                // `apply` is equivalent to passing each height as an argument
                maxHeight = Math.max.apply(null, elementHeights);
                // Set each height to the max height
                dis.find('.eq2').css('height',maxHeight);

                // j equal 3
                // Get an array of all element heights
                elementHeights = dis.find('.eq3').map(function() {
                    return $(this).outerHeight();
                }).get();
                // Math.max takes a variable number of arguments
                // `apply` is equivalent to passing each height as an argument
                maxHeight = Math.max.apply(null, elementHeights);
                // Set each height to the max height
                dis.find('.eq3').css('height',maxHeight);

                // j equal 4
                // Get an array of all element heights
                elementHeights = dis.find('.eq4').map(function() {
                    return $(this).outerHeight();
                }).get();
                // Math.max takes a variable number of arguments
                // `apply` is equivalent to passing each height as an argument
                maxHeight = Math.max.apply(null, elementHeights);
                // Set each height to the max height
                dis.find('.eq4').css('height',maxHeight);

                // Get an array of all element heights
                elementHeights = dis.find('.eq5').map(function() {
                    return $(this).outerHeight();
                }).get();
                // Math.max takes a variable number of arguments
                // `apply` is equivalent to passing each height as an argument
                maxHeight = Math.max.apply(null, elementHeights);
                // Set each height to the max height
                dis.find('.eq5').css('height',maxHeight);

                // Get an array of all element heights
                elementHeights = dis.find('.eq6').map(function() {
                    return $(this).outerHeight();
                }).get();
                // Math.max takes a variable number of arguments
                // `apply` is equivalent to passing each height as an argument
                maxHeight = Math.max.apply(null, elementHeights);
                // Set each height to the max height
                dis.find('.eq6').css('height',maxHeight);
            });
         }

        
        if($(window).width() >= 768 && $(window).width() <= 991){
            $('.eq-container.sm').each(function(){
                var dis = $(this),
                    elementHeights = false,
                    maxHeight = false;
                // Get an array of all element heights
                elementHeights = dis.find('.eq').map(function() {
                    return $(this).outerHeight();
                }).get();
                // Math.max takes a variable number of arguments
                // `apply` is equivalent to passing each height as an argument
                maxHeight = Math.max.apply(null, elementHeights);
                // Set each height to the max height
                dis.find('.eq').css('height',maxHeight);


                // j equal 2
                // Get an array of all element heights
                elementHeights = dis.find('.eq2').map(function() {
                    return $(this).outerHeight();
                }).get();
                // Math.max takes a variable number of arguments
                // `apply` is equivalent to passing each height as an argument
                maxHeight = Math.max.apply(null, elementHeights);
                // Set each height to the max height
                dis.find('.eq2').css('height',maxHeight);

                // j equal 3
                // Get an array of all element heights
                elementHeights = dis.find('.eq3').map(function() {
                    return $(this).outerHeight();
                }).get();
                // Math.max takes a variable number of arguments
                // `apply` is equivalent to passing each height as an argument
                maxHeight = Math.max.apply(null, elementHeights);
                // Set each height to the max height
                dis.find('.eq3').css('height',maxHeight);

                // j equal 4
                // Get an array of all element heights
                elementHeights = dis.find('.eq4').map(function() {
                    return $(this).outerHeight();
                }).get();
                // Math.max takes a variable number of arguments
                // `apply` is equivalent to passing each height as an argument
                maxHeight = Math.max.apply(null, elementHeights);
                // Set each height to the max height
                dis.find('.eq4').css('height',maxHeight);

                // Get an array of all element heights
                elementHeights = dis.find('.eq5').map(function() {
                    return $(this).outerHeight();
                }).get();
                // Math.max takes a variable number of arguments
                // `apply` is equivalent to passing each height as an argument
                maxHeight = Math.max.apply(null, elementHeights);
                // Set each height to the max height
                dis.find('.eq5').css('height',maxHeight);

                // Get an array of all element heights
                elementHeights = dis.find('.eq6').map(function() {
                    return $(this).outerHeight();
                }).get();
                // Math.max takes a variable number of arguments
                // `apply` is equivalent to passing each height as an argument
                maxHeight = Math.max.apply(null, elementHeights);
                // Set each height to the max height
                dis.find('.eq6').css('height',maxHeight);
            });
         }

            if($(window).width() <= 767){
                $('.eq-container.xs').each(function(){
                    var dis = $(this),
                    elementHeights = false,
                    maxHeight = false;
                    // Get an array of all element heights
                    elementHeights = dis.find('.eq').map(function() {
                        return $(this).outerHeight();
                    }).get();
                    // Math.max takes a variable number of arguments
                    // `apply` is equivalent to passing each height as an argument
                    maxHeight = Math.max.apply(null, elementHeights);
                    // Set each height to the max height
                    dis.find('.eq').css('height',maxHeight);


                    // j equal 2
                    // Get an array of all element heights
                    elementHeights = dis.find('.eq2').map(function() {
                        return $(this).outerHeight();
                    }).get();
                    // Math.max takes a variable number of arguments
                    // `apply` is equivalent to passing each height as an argument
                    maxHeight = Math.max.apply(null, elementHeights);
                    // Set each height to the max height
                    dis.find('.eq2').css('height',maxHeight);

                    // j equal 3
                    // Get an array of all element heights
                    elementHeights = dis.find('.eq3').map(function() {
                        return $(this).outerHeight();
                    }).get();
                    // Math.max takes a variable number of arguments
                    // `apply` is equivalent to passing each height as an argument
                    maxHeight = Math.max.apply(null, elementHeights);
                    // Set each height to the max height
                    dis.find('.eq3').css('height',maxHeight);

                    // j equal 4
                    // Get an array of all element heights
                    elementHeights = dis.find('.eq4').map(function() {
                        return $(this).outerHeight();
                    }).get();
                    // Math.max takes a variable number of arguments
                    // `apply` is equivalent to passing each height as an argument
                    maxHeight = Math.max.apply(null, elementHeights);
                    // Set each height to the max height
                    dis.find('.eq4').css('height',maxHeight);

                    // Get an array of all element heights
                    elementHeights = dis.find('.eq5').map(function() {
                        return $(this).outerHeight();
                    }).get();
                    // Math.max takes a variable number of arguments
                    // `apply` is equivalent to passing each height as an argument
                    maxHeight = Math.max.apply(null, elementHeights);
                    // Set each height to the max height
                    dis.find('.eq5').css('height',maxHeight);

                    // Get an array of all element heights
                    elementHeights = dis.find('.eq6').map(function() {
                        return $(this).outerHeight();
                    }).get();
                    // Math.max takes a variable number of arguments
                    // `apply` is equivalent to passing each height as an argument
                    maxHeight = Math.max.apply(null, elementHeights);
                    // Set each height to the max height
                    dis.find('.eq6').css('height',maxHeight);
                });
             }

        switch_contents();
    }

    // S W I T C H E D  C O N T E N T S  C O M P O N E N T
    function switch_contents(){
        if($(window).width() >= 768 && $(window).width() <= 991){
            $('.switch-contents-xs').each(function(){
                $(this).find('or1').insertBefore($(this).find('or2'));
            });
            $('.switch-contents-sm').each(function(){
                $(this).find('or1').insertAfter($(this).find('or2'));
            });
        }
        if($(window).width() <= 767){
            $('.switch-contents-sm').each(function(){
                $(this).find('or1').insertBefore($(this).find('or2'));
            });
            $('.switch-contents-xs').each(function(){
                $(this).find('or1').insertAfter($(this).find('or2'));
            });
        }
        if($(window).width() >= 992){
            $('.switch-contents-sm,.switch-contents-xs').each(function(){
                $(this).find('or1').insertBefore($(this).find('or2'));
            })
        }
    }


    $(window).on('load',function(e){
        equal();
    });
    $(window).on('resize',function(e){
        equal();
    });

    /* ------------------------------------------------------------ */
    /*                 C O N T E N T  L O A D E R                   */
    /* ------------------------------------------------------------ */
    $(function(){
        $('.content-spinner').each(function(){
            let spinner = 0;
            if( $(this).attr('data-spinner') ){

                spinner = $(this).attr('data-spinner');
            }

            $(this).css('height',$(this).attr('data-height')+'px');
            $(this).html(spinnerHTML(spinner));
        });

    });

}else{

    console.log('jQuery is required');

}

