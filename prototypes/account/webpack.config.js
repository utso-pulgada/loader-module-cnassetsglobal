const ImageminWebpWebpackPlugin= require("imagemin-webp-webpack-plugin");

module.exports = {
	publicPath: 'http://localhost:8000/account/',
    plugins: [new ImageminWebpWebpackPlugin()]
};