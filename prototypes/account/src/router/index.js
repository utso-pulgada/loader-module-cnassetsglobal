import Vue from 'vue';
import Router from 'vue-router';
import signin from './../pages/signin.vue';
import signup from './../pages/signup.vue';
import forgot from './../pages/forgotpassword.vue';
import verify from './../pages/verifyaccount.vue';


Vue.use(Router);

export default new Router({

	routes : [
		{
			path : '/',
			name : 'signin',
			component : signin
		},
		{
			path : '/signin',
			name : 'signin',
			component : signin
		},
		{
			path : '/signup',
			name : 'signup',
			component : signup
		},
		{
			path : '/forgot',
			name : 'forgot',
			component : forgot
		},
		{
			path : '/verify',
			name : 'verify',
			component : verify
		},
	],
	// mode : 'history'

})

