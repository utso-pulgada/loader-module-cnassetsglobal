<?php

	namespace PDLoader;


	class Cnglobalassets{


		public function onBuild(){
			if( file_exists(__DIR__.'/package.json') ){
				unlink(__DIR__.'/package.json');
			}
			if( file_exists(__DIR__.'/package-lock.json') ){
				unlink(__DIR__.'/package-lock.json');
			}
			
			if( file_exists(__DIR__.'/.gitignore') ){
				unlink(__DIR__.'/.gitignore');
			}

			if( file_exists(__DIR__.'/prototypes') ){
				$this->deleteFolder(__DIR__.'/prototypes');
			}

			if( file_exists(__DIR__.'/.phpintel') ){
				$this->deleteFolder(__DIR__.'/.phpintel');
			}
			if( file_exists(__DIR__.'/.cache') ){
				$this->deleteFolder(__DIR__.'/.cache');
			}
			if( file_exists(__DIR__.'/src') ){
				$this->deleteFolder(__DIR__.'/.src');
			}

		}
		private function deleteFolder($dir){
			if(file_exists($dir)){
				$it = new \RecursiveDirectoryIterator($dir, \RecursiveDirectoryIterator::SKIP_DOTS);
				$files = new \RecursiveIteratorIterator($it,
				             \RecursiveIteratorIterator::CHILD_FIRST);

				foreach($files as $file) {
					chmod($file->getRealPath(),0755);
				    if ($file->isDir()){
				        rmdir($file->getRealPath());
				    } else {
				        unlink($file->getRealPath());
				    }
				}
				rmdir($dir);
			}
			
		}	

	}